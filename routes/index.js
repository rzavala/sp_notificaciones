/**********************************************************************************************
 *                        Centro de Ingeniería y Desarrollo Industrial
 *
 * Nombre del Aplicativo              : Envío de notificaciones por correo electrónico.
 * Archivo                            : index.js
 * Lenguaje                           : JavaScript
 * Propósito                          : Implementar funcionalidad para enviar correos elctrónicos
 *                                      a usuarios del sistema.
 *
 * Historia...
 * Fecha de Creacion/Modificacion     : Jueves, 02 de Febrero de 2017.
 * Responsable Última Modificación    : Antonio Trejo Morales
 *
 ********************* Copyright (C) 2017 CIDESI - http://www.cidesi.com/ *********************/

'use strict';

var request = require('request');
var nodemailer = require('nodemailer');

// create reusable transporter object using the default SMTP transport
const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: global.config.emailServer,
        pass: global.config.passwordServer
    }
});

// setup email data with unicode symbols
var mailOptions = {
    from: global.config.emailServer,            // sender address
    to: 'atrejo@cidesi.edu.mx',                 // list of receivers
    subject: '👻 Seguimiento de proyectos.',    // Subject line
    text: 'Hello world ?',                      // plain text body
    html: ''                                    // html body
};

//✔

/**********************************************************************************************
 * Propósito                        	: Función para envío de notificaciones de errores.
 *
 * Parámetros In    					: JSON: notify =
 *                                        {
 *                                          'mensaje': 'Mongo error',
 *                                          'fecha': '24/08/16',
 *                                          'microservicio': 'MongoDB'
 *                                        }
 * Parámetros Out 					    : JSON: msj =
 *                                        {
 *                                          'response': 200,
 *                                          'message': '',
 *                                          'error': ''
 *                                          }
 *
 * Fecha de Creacion/Modificacion   	: Jueves, 02 de Febrero de 2017.
 * Responsable Ultima Modificación  	: Trejo Morales Antonio.
 *
 ******************Copyright (C) 2017 CIDESI - http://www.cidesi.edu.com/ *********************/

function notifyError (notify) {
    return new Promise(function(resolve, reject){
        var html = '';
        mailOptions.to = global.config.emailAdmin;
        mailOptions.subject = "Notificación de errores";

        html = global.config.htmlMsjError;
        html = html.replace('KeyProject', '');
        html = html.replace('keyMsj', notify.mensaje);
        html = html.replace('keyDate', notify.fecha);
        html = html.replace('keyService', notify.microservicio);

        mailOptions.html = global.config.htmlTitle;
        mailOptions.html += html;
        mailOptions.html += global.config.htmlStatus;

        // send mail with defined transport object
        transporter.sendMail(mailOptions, function (error, info){
            var msj = {'response': 200, 'message': '', 'error': ''};

            if(error){
                msj.response = 401;
                msj.message = 'No se envío la notificación de errores al e-mail: ' + global.config.emailAdmin;
                msj.error = error;

                global.config.debug && console.log('In function notifyError(... Error:',msj);
                reject(msj);
            }
            else{
                //global.config.debug && console.log('Message %s sent: %s', info.messageId, info.response);
                msj.message = 'Correo con notificaciones de errores enviado con éxito al e-mail: ' + mailOptions.to;
                global.config.debug && console.log('In function notifyError(...: ',msj);
                resolve(msj);
            }
        });
    });
}

/**********************************************************************************************
 * Propósito                        	: Función para construir el HTML con el listado de
 *                                        las actividades correspondientes a cada proyecto.
 *
 * Parámetros In    					: JSON: activities =
 *                                        {
 *                                          'actividad'         : 'Ajustes y pruebas...',
 *                                          'fechacompromiso'   : '24/08/17',
 *                                          'impacto'           : 1,
 *                                          'ent_evi'           : 'Reporte técnico'
 *                                        }
 *
 * Parámetros Out 					    : Variable String:  htmlActivities.
 *
 * Fecha de Creacion/Modificacion   	: Viernes, 03 de Febrero de 2017.
 * Responsable Ultima Modificación  	: Trejo Morales Antonio.
 *
 ******************Copyright (C) 2017 CIDESI - http://www.cidesi.edu.com/ *********************/

function getActivity (activities) {
    return new Promise(function(resolve, reject){
        var html = '';
        var htmlActivities = '';
        var nLen = activities.length;

        activities.forEach(function(activity){
            html = global.config.htmlActivities;
            html = html.replace('keyActivity', activity.actividad);
            html = html.replace('keyImpact', activity.impacto);
            html = html.replace('keyDate', activity.fechacompromiso);
            html = html.replace('keyEvidence', activity.ent_evi);
            htmlActivities += html;

            nLen--;
            if(nLen <= 0){
                resolve(htmlActivities);
            }
        });

    });
}

/**********************************************************************************************
 * Propósito                        	: Función para construir el HTML con el listado de
 *                                        los proyectos con sus actividades.
 *
 * Parámetros In    					: JSON: projects =
 *                                        {
 *                                          'nombreProyecto'    : 'Ajustes y pruebas...',
 *                                          'email'             : 'atrejo@cidesi.edu.mx',
 *                                          'actividades'       : [Object Object]
 *                                        }
 *
 * Parámetros Out 					    : Variable String:  htmlProjects.
 *
 * Fecha de Creacion/Modificacion   	: Viernes, 03 de Febrero de 2017.
 * Responsable Ultima Modificación  	: Trejo Morales Antonio.
 *
 ******************Copyright (C) 2017 CIDESI - http://www.cidesi.edu.com/ *********************/

function getProject (projects) {
    return new Promise(function(resolve, reject){
        var html = '';
        var htmlProjects = '';
        var nLen = projects.length;

        projects.forEach(function(projects){
            getActivity(projects.actividades)
                .then(function(htmlActivities){
                    nLen--;
                    html = global.config.htmlProjects;
                    html = html.replace('KeyProject', projects.nombreProyecto);
                    htmlProjects = htmlProjects + html + htmlActivities + global.config.htmlCloseProjects;
                    if(nLen <= 0){
                        resolve(htmlProjects);
                    }
                })
                .catch(function (err){
                    nLen--;
                    if(nLen <= 0){
                        global.config.debug && console.log('In function getProject(... Error en la Promise getActivity() ' , err);
                        reject(htmlProjects);
                    }
                });
        });
    });
}

/**********************************************************************************************
 * Propósito                        	: Función para enviar por e-mail los proyectos/actividades
 *                                        a cada usuario.
 *
 * Parámetros In    					: JSON: userProjects =
 *                                        {
 *                                          usuario : [Object Object]
 *                                        }
 *
 * Parámetros Out 					    : JSON: msj =
 *                                        {
 *                                          'response': 200,
 *                                          'message': '',
 *                                          'error': ''
 *                                          }
 *
 * Fecha de Creacion/Modificacion   	: Jueves, 02 de Febrero de 2017.
 * Responsable Ultima Modificación  	: Trejo Morales Antonio.
 *
 ******************Copyright (C) 2017 CIDESI - http://www.cidesi.edu.com/ *********************/

function sendEmails (userProjects) {
    return new Promise(function(resolve, reject){
        mailOptions.html ='';
        mailOptions.to = userProjects[0].email;
        var msj = {'response': 200, 'message': '', 'error': ''};

        getProject(userProjects)
            .then(function (htmlProjects){
                mailOptions.html = global.config.htmlTitle + htmlProjects + global.config.htmlStatus;
                // send mail with defined transport object

                // Validar que si existe un error al enviar el email, entonces,
                // hacer el reintento de envío por 2da. vez,

                transporter.sendMail(mailOptions, function (error, info){
                    var msj = {'response': 200, 'message': '', 'error': ''};

                    if(error){
                        transporter.sendMail(mailOptions, function (error, info){
                            var msj2 = {'response': 200, 'message': '', 'error': ''};

                            if(error){
                                msj2.response = 401;
                                msj2.message = 'Error al enviar el correo electrónico de: ' + mailOptions.to;
                                msj2.error = error;

                                global.config.debug && console.log('In function sendEmails(... Error:',msj2);
                                reject(msj2);
                            }
                            else{
                                //global.config.debug && console.log('Message %s sent: %s', info.messageId, info.response);
                                global.config.debug && console.log('Message send to: %s', mailOptions.to);
                                resolve(msj2);
                            }
                        });
                    }
                    else{
                        //global.config.debug && console.log('Message %s sent: %s', info.messageId, info.response);
                        global.config.debug && console.log('Message send to: %s', mailOptions.to);
                        resolve(msj);
                    }
                });
            })
            .catch(function (err){
                msj.response = 401;
                msj.message = 'No se enviaron los proyectos/actividades al e-mail: ' + mailOptions.to;
                msj.error= 'Ocurrió algún error en la Promise getProject(userProjects):' + err;

                global.config.debug && console.log('In function sendEmails(... Error:', msj);
                reject(msj);
            });
    });
}

/**********************************************************************************************
 * Propósito                        	: Función para solicitar las actividades del usuario
 *                                        al Microservicio MongoDB
 *                                        (http://localhost:8082/api/proyecto/actividad).
 *
 * Parámetros In    					: Variable: idUser
 * Parámetros Out 					    : Variable: JSON con actividades del usuario.
 *
 * Fecha de Creacion/Modificacion   	: Jueves, 02 de Febrero de 2017.
 * Responsable Ultima Modificación  	: Trejo Morales Antonio.
 *
 ******************Copyright (C) 2017 CIDESI - http://www.cidesi.edu.com/ *********************/

function getUserActivity (idUser) {
    return new Promise(function(resolve, reject){
        var msj;
        var url = global.config.urlMongoDBUserActivity + '?codigousuario=' + idUser;

        // Llamar por el método GET al microservicio que retorna las actividades del usuario
        request.get(url,function(error, req, res){
            var userActivity = JSON.parse(req.body);

            if(error){
                msj = {
                    'response': 401,
                    'message' : 'Error al obtener las actividades ' +
                    'de usuario del microservicio: '+
                    global.config.urlMongoDBUserActivity,
                    'error'   : error
                };
                global.config.debug && console.log('In function getUserActivity()... Error: ', msj);
                reject (msj);
            }else{
                resolve(userActivity.proyectos);
            }
        });
    });
}

/**********************************************************************************************
 * Propósito                        	: Función para obtener el email de cada usuario.
 *
 * Parámetros In    					: Variable: JSON con lista de usuarios.
 *                                          “usuarios”:[
 {“codigoEmpleado”: “177292”,
  “email”: “nataly.contreras@cidesi.edu.mx”,
  “prefijo”: [ “GKN”, “CRUM”, “MONFET”]
 },
 {“codigoEmpleado”: “09115”,
  “email”: “atrejo@cidesi.edu.mx”,
  “prefijo”: [ “GKN”, “CRUM”, “MONFET”]
 },... ,
 ]
 * Parámetros Out 					    : Variable: JSON con datos de respuesta.
 *
 * Fecha de Creacion/Modificacion   	: Jueves, 02 de Febrero de 2017.
 * Responsable Ultima Modificación  	: Trejo Morales Antonio.
 *
 ******************Copyright (C) 2017 CIDESI - http://www.cidesi.edu.com/ *********************/

function getUsers(usersList) {
    return new Promise(function(resolve, reject) {
        var nLen = usersList.usuarios.length;
        var msj = {'response': 200, 'message': 'Notificaciones enviadas con éxito', 'error': ''};

        usersList.usuarios.forEach(function(user){
            global.config.debug && console.log('In function getUsers(: ' + user.codigoEmpleado, user.email);
            getUserActivity(user.codigoEmpleado)
                .then(function(userActivity){
                    //global.config.debug && console.log(userActivity);
                    return sendEmails(userActivity);
                })
                .then(function(){ // Caso verdadrero de la Promesa de sendEmail()
                    nLen--;
                    if(nLen<=0)
                        resolve(msj);
                })
                .catch(function (err){
                    nLen--;
                    global.config.debug && console.log('In function getUsers(... Error: ', err);
                    if(nLen<=0)
                        resolve(err);
                });
        });
    });
}

/**********************************************************************************************
 * Propósito                        	: Función para solicitar los E-Mails al Microservicio.
 *                                        Mongo DB (http://localhost:8082/api/email/).
 *
 * Parámetros In    					: Función callback 'next'.
 * Parámetros Out 					    : None.
 *
 * Fecha de Creacion/Modificacion   	: Jueves, 02 de Febrero de 2017.
 * Responsable Ultima Modificación  	: Trejo Morales Antonio.
 *
 ******************Copyright (C) 2017 CIDESI - http://www.cidesi.edu.com/ *********************/

function getUsersList(req) {
    return new Promise(function(resolve, reject) {
        // Llamar por el método GET al microservicio que retorna la lista de usuarios
        request.get(global.config.urlMongoDBUsers,function(error, req, res){
            var msj;
            var usersList ='';

            if(error){
                msj = {
                    'response': 404,
                    'message' : 'Error al obtener la lista de usuarios del microservicio: '+ global.config.urlMongoDBUsers,
                    'error'   : error
                };
                global.config.debug && console.log('function getUsersList(req)... Error:',msj);
                reject(msj);
            }else{
                usersList = JSON.parse(req.body);
                //global.config.debug && console.log('function getUsersList(req)...',usersList);
                resolve(usersList);
            }
        });
    });
}

/**********************************************************************************************
 * Propósito                        	: Función principal del módulo de notificaciones.
 *
 * Parámetros In    					: None.
 * Parámetros Out 					    : Response.
 *
 * Fecha de Creacion/Modificacion   	: Jueves, 02 de Febrero de 2017.
 * Responsable Ultima Modificación  	: Trejo Morales Antonio.
 *
 ******************Copyright (C) 2017 CIDESI - http://www.cidesi.edu.com/ *********************/

module.exports = {
    send: function(next) {

        getUsersList('')
            .then(function(usersList){
                return getUsers(usersList);
            })
            .then(function(res){
                next(res);
            })
            .catch(function (err){
                next(err);
            });
    },
    notificarError: function(notify, next) {
        var msj = {'response': 401, 'message': '', 'error': ''};
        global.config.debug && console.log('In notificarError ()... Mensaje a notificar: ', notify);

        if(notify.fecha === undefined || notify.microservicio === undefined || notify.mensaje === undefined)
        {
            msj.message = 'Se esperaba un JSON que contenga las llaves: fecha, microservicio y mensaje';
            msj.error = 'No se envío la notificación de error al e-mail: ' + global.config.emailAdmin;
            global.config.debug && console.log('In notificarError (... Error:', msj);
            next(msj);
        }else{
            notifyError(notify)
                .then(function(response){   // Caso verdadero de la promesa: notifyError()
                    next(response);
                })
                .catch(function(err){       // Caso falso de la promesa: notifyError()
                    next(err);
                });
        }
    }
};
