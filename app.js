
/**********************************************************************************************
 *                        Centro de Ingeniería y Desarrollo Industrial
 *
 * Nombre del Aplicativo              : Envío de notificaciones por correo electrónico.
 * Archivo                            : app.js
 * Lenguaje                           : JavaScript
 * Propósito                          : Tarea programada que implementa funcionalidad para
 *                                      enviar las activiades diarias via correos electrónicos
 *                                      a los trabajadores.
 *
 * Historia...
 * Fecha de Creacion/Modificacion     : Jueves, 02 de Febrero de 2017.
 * Responsable Última Modificación    : Antonio Trejo Morales
 *
 ********************* Copyright (C) 2017 CIDESI - http://www.cidesi.com/ *********************/

var express = require('express');
var schedule = require('node-schedule');
var bodyParser = require('body-parser');
global.config=require('./config.json');
var notificaciones=require('./routes/index.js');
var app = express();


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

var server=app.listen(global.config.puerto, function () {
    var port = server.address().port
    console.log("Escuchando en puerto: "+port + '\n');
    global.config.debug && console.log('Resources:');
    global.config.debug && console.log('[POST] http://localhost:8004/api/error \n');
    //global.config.debug && console.log('http://localhost:8004/enviarNotificacion \n');
});


/**********************************************************************************************
 * * Propósito                        	: Petición REST tipo GET para envío de notificaciones
 *                                        de tareas pendientes de realizar por poyectos.
 *
 * Parámetros In    					: None.
 * Parámetros Out 					    : Response.
 *
 * Fecha de Creacion/Modificacion   	: Viernes, 03 de Febrero de 2017.
 * Responsable Ultima Modificación  	: Trejo Morales Antonio.
 *
 * ******************Copyright (C) 2017 CIDESI - http://www.cidesi.edu.com/ *********************/

app.get('/enviarNotificacion',function(req,res){
    notificaciones.send(function(response){
        res.send(response);
    });
});

/**********************************************************************************************
 * Propósito                        	: Petición REST tipo POST para envío de notificaciones
 *                                        por bugs.
 *
 * Parámetros In    					: Variable JSON:
 *                                        {
 *                                             error: 'Mongo error',
 *                                             fecha: '24/08/16',
 *                                             microservicio: 'MongoDB'
 *                                        };
 * Parámetros Out 					    : Response.
 *
 * Fecha de Creacion/Modificacion   	: Viernes, 03 de Febrero de 2017.
 * Responsable Ultima Modificación  	: Trejo Morales Antonio.
 *
 ******************Copyright (C) 2017 CIDESI - http://www.cidesi.edu.com/ *********************/

app.post('/api/error',function(req,res){
    var notify = req.body;
    //global.config.debug && console.log('In function app.post(/api/error... ', notify);

    notificaciones.notificarError(notify,function(response){
        res.send(response);
    });

});

/**********************************************************************************************
 * Propósito                        	: Tarea programada para envío de notificaciones diarias
 *
 * Parámetros In    					: Tiempo: cada 24hrs. a las 07:00:00 hrs.
 *                                            Node Schedule
 *                                      scheduleJob ( *    *    *    *    *    * )
 *                                                    ┬    ┬    ┬    ┬    ┬    ┬
 *                                                    │    │    │    │    │    |
 *                                                    │    │    │    │    │    └ day of week (0 - 7) (0 or 7 is Sun)
 *                                                    │    │    │    │    └───── month (1 - 12)
 *                                                    │    │    │    └────────── day of month (1 - 31)
 *                                                    │    │    └─────────────── hour (0 - 23)
 *                                                    │    └──────────────────── minute (0 - 59)
 *                                                    └───────────────────────── second (0 - 59, OPTIONAL)
 * Parámetros Out 					    : Response.
 *
 * Fecha de Creacion/Modificacion   	: Viernes, 03 de Febrero de 2017.
 * Responsable Ultima Modificación  	: Trejo Morales Antonio.
 *
 ******************Copyright (C) 2017 CIDESI - http://www.cidesi.edu.com/ *********************/

var task = schedule.scheduleJob(global.config.timeTask, function(){
    notificaciones.send(function(response){
        console.log('In function schedule.scheduleJob(...: ', response);
    });
});


